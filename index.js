var rulesList = [];

var checkAccess = function (req, res, next) {
	var path = req.path,
		rule, i,
		userroles = req.session.roles ? req.session.roles : null;
	if (path.match(/\.(css|png|jpg|jpeg|js)$/i)) {
		next();
		return;
	}

	for (i = 0; i < rulesList.length; i++) {
		rule = rulesList[i];
		if (path.match(rule.route)) {
			if (haveRoles(rule.roles, userroles)) {
				next();
				return;
			} else {
				res.redirect("/index?restrict");
				return;
			}
		}
	}
	next();
};
var haveRoles = function (list, roles) {
	if (roles === null || typeof roles === "string") {
		return haveRole(list, roles);
	} else if (typeof roles === "object" && roles instanceof Array) {
		for (var i = 0; i < roles.length; i++) {
			if (haveRole(list, roles[i])) {
				return true;
			}
		}
		return false;
	}
};
var haveRole = function (list, role) {
	if (list === null) {
		return true;
	} else if (role === null) {
		return false;
	} else if (typeof list === "string") {
		return (list === role);
	} else if (typeof list === "object" && list instanceof Array) {
		for (var i = 0; i < list.length; i++) {
			if (list[i] === role) {
				return true;
			}
		}
		return false;
	}
	return false;
};

var Module = {
	init : function (app) {
		app.all("/*", checkAccess);
	},
	injectRules : function (rules) {
		for (var i = 0; i < rules.length; i++) {
			rulesList.push(rules[i]);
		}
	}
};

module.exports = Module;